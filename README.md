# GeoQuigiz 
### A QGIS plugin

###### With this plugin, it is intended that, through a button in QGIS, the user can play 2 games, related to the countries/capitals of the world, and their flags.

###### In a first game "Capitals' Game", where the world map appears and a window appears with a random capital, the player will have to click on the point of the map where he thinks it is the respective capital.

###### In the second game "Flags' Game", a window appears with a random flag, and the player will have to click on the point of the map where they think it is the country with their flag. 

###### In both games, the player has 3 tries and the last, if he misses, the correct location appears, moving on to the next question. A total of five questions are asked per game. As you hit, the locations are recorded on the map.




```                                 
*******************************************************************************************************************************************
*******************************************************************************************************************************************
*                                                                                                                                         *
*                                                    MIEBIOM – Informática Médica – 2020/2021                                             *
*                                                       Sistemas de Informação Geográfica                                                 *
*                                                                                                                                         *
*******************************************************************************************************************************************
*                                                                                                                                         *
*   João Miguel da Silva Alves (83624)                                                                                                    *
*   Paulo Jorge Alves (84480)                                                                                                             *
*                                                                                                                                         *
*******************************************************************************************************************************************
*******************************************************************************************************************************************
``` 

